from conans import ConanFile, CMake, tools

class FooConan(ConanFile):
    name = "Foo"
    version = "0.1"
    author = "Peter Lukacs lukacs.peter19@gmail.com"
    url = "https://gitlab.com/spektrof/conan-foo"
    description = "Foo library"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"

    requires = ["Bar/0.1@spektrof+conan-bar/beta"]

    def configure(self):
        if self.options.shared:
            self.options.fPIC=False

    def source(self):
        self.run("git clone https://gitlab.com/spektrof/conan-foo.git")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="conan-foo")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="conan-foo")
        self.copy("*.so", dst="lib", src="lib")
        self.copy("*.a", dst="lib", src="lib")

    def package_info(self):
        self.cpp_info.libdirs = ["lib"]
        self.cpp_info.libs = ["foo"]
        self.cpp_info.includedirs = ["include"]
        self.cpp_info.requires = ["Bar::bar"]

